<?php
define('STRIPE_SECRET_KEY','sk_test_oPnVxf1446pazORycVMgDKH8');
define('STRIPE_PUBLISHABLE_KEY','pk_test_Zpc1E00DeG2HlqRqzcw2Hsu6');
header('Content-Type: application/json');
$results = array();
require 'vendor/autoload.php';
\Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
if(isset($_POST['method'])){
    $method = $_POST['method'];
    if($method =="charge"){
        $email = $_POST['email'];
        $source = $_POST['source'];
        try{
            $customer = \Stripe\Customer::create([
              "email" => $email,
              "source" => $source
            ]);
            $amount = $_POST['amount'];
            $currency = $_POST['currency'];
            $description = $_POST['description'];            
            $mac = $_POST['mac'];
            $firmware = $_POST['firmware'];
            $otaid = $_POST['otaid'];
            try {
                $charge = \Stripe\Charge::create(array(
                    "amount" => $amount, // Amount in cents
                    "currency" => $currency,
                    "customer" => $customer->id,
                    "description" => $description,
                    "metadata" => array("mac" => $mac, "firmware" => $firmware, "otaid" => $otaid)
                ));
                if($charge!=null){
                    $results['response'] = "Success";
                    $results['message'] = "Charge has been completed";
                }
            } catch(\Stripe\Error\Card $e) {
                $results['response'] = "Error";
                $results['message'] = $e->getMessage();
            }
        } catch (Exception $e){
            $results['response'] = "Error";
            $results['message'] = $e->getMessage();
        }

        echo json_encode($results);
    }else {
        $results['response'] = "Error";
        $results['messsage'] = "Method name is not correct";
        echo json_encode($results);
    }
}else {
    $results['response'] = "Error";
    $results['message'] = "No method has been set";
    echo json_encode($results);
}
