package android.stripe.dongletv;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class CustomerActivity extends AppCompatActivity {

    EditText editEmail;
    String email, message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);

        editEmail = findViewById(R.id.input_email);
    }


    public void createCustomer(View view) {

        email = editEmail.getText().toString();

        if(email.equals("")) {
            Toast.makeText(this, "Please input your email.", Toast.LENGTH_SHORT).show();
            return;
        }

        new CreateCustomer().execute();
    }


    public class CreateCustomer extends AsyncTask<String, Void, String>{

        private Handler handler = new Handler();

        private ProgressDialog progress = new ProgressDialog(CustomerActivity.this);
        @Override
        protected String doInBackground(String... strings) {
            new Thread() {
                @Override
                public void run() {

                    try {
                        URL url = new URL("https://stripe.android.dongletv.com/customer.php");
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setReadTimeout(10000);
                        conn.setConnectTimeout(15000);
                        conn.setRequestMethod("POST");
                        conn.setDoInput(true);
                        conn.setDoOutput(true);

                        List<NameValuePair> params = new ArrayList<>();
                        params.add(new NameValuePair("method", "customer"));
                        params.add(new NameValuePair("email", email));


                        OutputStream os = null;

                        os = conn.getOutputStream();
                        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                        writer.write(getQuery(params));
                        writer.flush();
                        writer.close();


                        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        String decodedString;
                        StringBuilder stringBuilder = new StringBuilder();
                        while ((decodedString = in.readLine()) != null) {
                            stringBuilder.append(decodedString);
                        }
                        in.close();
                        /*YOUR RESPONSE */
                        String json = stringBuilder.toString();
                        JSONObject jObj = new JSONObject(json);
                        String response = jObj.getString("response");
                        os.close();

                        // hideProgressDialog
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                // Update the progress status
                                progress.dismiss();
                            }
                        });

                        message = jObj.getString("message");
                        if(response.equals("Success")){
                            Intent cardIntent = new Intent(CustomerActivity.this, CardActivity.class);
                            cardIntent.putExtra("customer", jObj.getString("message"));
                            startActivity(cardIntent);
                            finish();
                        } else {
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(CustomerActivity.this, message, Toast.LENGTH_LONG).show();
                                }
                            });
                        }

                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                }
            }.start();
            return "Done";
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.e("Result",s);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            progress.setCancelable(true);
            progress.setTitle("Customer");
            progress.setMessage("Progress...");
            progress.show();

        }
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }
        Log.e("Query",result.toString());
        return result.toString();
    }


}
