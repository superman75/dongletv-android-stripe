package android.stripe.dongletv;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class MainActivity extends AppCompatActivity {

    ArrayList<SimplePlan> planArrayList;
    RecyclerView recyclerView;
    ItemsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        planArrayList = new ArrayList<>();
        new Async().execute();
    }

    public void showRcv(ArrayList<SimplePlan> plans){
        adapter = new ItemsAdapter(this,plans);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }


    public class Async extends AsyncTask<Void,String,ArrayList<SimplePlan>> {

        @Override
        protected ArrayList<SimplePlan> doInBackground(Void... params) {

            try {
                String line;StringBuilder newjson = new StringBuilder();
                URL url = new URL("https://stripe.android.dongletv.com/getskus.php");
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
                    while ((line = reader.readLine()) != null) {
                        newjson.append(line);
                    }
                    String json = newjson.toString();
                    JSONObject jObj = new JSONObject(json);
                    Log.e("Obj",jObj.toString());
                    JSONArray plans = jObj.getJSONArray("data");
                    for (int i=0;i<plans.length();i++){
                        JSONObject plan = plans.getJSONObject(i);
                        planArrayList.add(new SimplePlan(plan.getInt("price"),plan.getString("name"),plan.getString("description"), plan.getString("image"), plan.getString("currency")));
                    }

                    //get stripe public key from server and save to local Storage
                    String stripe_public_key = jObj.getString("public_key");
                    SharedPreferences myPreferences
                            = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                    SharedPreferences.Editor myEditor = myPreferences.edit();
                    myEditor.putString("public_key", stripe_public_key);
                    myEditor.apply();


                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return planArrayList;
        }

        @Override
        protected void onPostExecute(final ArrayList<SimplePlan> plan) {
            super.onPostExecute(plan);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showRcv(plan);
                }
            },500);
        }
    }

    public class SimplePlan{
        String  name, description, image, currency;
        Integer amount;


        public SimplePlan(Integer amount, String name, String description, String image, String currency) {
            this.amount = amount/100;
            this.name = name;
            this.description = description;
            this.image = image;
            this.currency = currency;
        }



        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }
    }
}
