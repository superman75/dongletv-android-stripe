package android.stripe.dongletv;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AppCompatActivity;

public class ConfirmActivity extends AppCompatActivity {
    TextView product_name, product_desc, mac_address, firmware_address, otaid_address;
    ImageView product_image;
    String productName, productDesc, productImage;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
        product_name = findViewById(R.id.prod_name);
        product_desc = findViewById(R.id.prod_description);
        product_image = findViewById(R.id.prod_image);
        mac_address = findViewById(R.id.mac_address);
        firmware_address = findViewById(R.id.firmware);
        otaid_address = findViewById(R.id.otaid);

        SharedPreferences myPreferences
                = PreferenceManager.getDefaultSharedPreferences(this);

        productName = myPreferences.getString("productName","Product Name");
        productDesc = myPreferences.getString("productDesc", "Product Description");
        productImage = myPreferences.getString("productImage", "");

        product_name.setText(productName);
        product_desc.setText(productDesc);
        Picasso.with(this).load(productImage).into(product_image);


        DeviceInfo deviceInfo = new DeviceInfo();
        mac_address.setText("mac " + deviceInfo.getMacAddressValue());
        firmware_address.setText("firmware " + deviceInfo.getBuildPropValues("ro.build.display.id"));
        otaid_address.setText("otaid" + deviceInfo.getBuildPropValues("otaupdater.otaid"));

    }

    public void finish(View view) {
        finish();
        System.exit(0);
    }
}
