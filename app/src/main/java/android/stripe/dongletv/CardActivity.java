package android.stripe.dongletv;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

public class CardActivity extends AppCompatActivity {

    EditText  editFName, editLName, editCountry, editCity, editState, editZipcode, editAddress;
    String currency, country, city, state, zipcode, address, firstName, lastName, customer;
    ProgressDialog progress;
    Stripe stripe;
    String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);


        // get stripe public key from local storage
        SharedPreferences myPreferences
                = PreferenceManager.getDefaultSharedPreferences(this);
        String public_key = myPreferences.getString("public_key", "pk_test_Zpc1E00DeG2HlqRqzcw2Hsu6");
        stripe = new Stripe(this, public_key);

        editAddress = findViewById(R.id.input_address);
        editCountry = findViewById(R.id.input_country);
        editCity = findViewById(R.id.input_city);
        editState = findViewById(R.id.input_state);
        editZipcode = findViewById(R.id.input_zipcode);
        editFName = findViewById(R.id.input_firstname);
        editLName = findViewById(R.id.input_lastname);
        editFName.requestFocus();

        Bundle extras = getIntent().getExtras();
        customer = extras.getString("customer");

    }

    public void submitCard(View view) {

        CardInputWidget mCardInputWidget = findViewById(R.id.card_input_widget);
        Card card = mCardInputWidget.getCard();
        if (card == null) {
            // Do not continue token creation.
            Toast.makeText(this, "Please input valid card.", Toast.LENGTH_LONG).show();
            return;
        }

        firstName = editFName.getText().toString();
        lastName = editLName.getText().toString();
        country = editCountry.getText().toString();
        city = editCity.getText().toString();
        state = editState.getText().toString();
        zipcode = editZipcode.getText().toString();
        address = editAddress.getText().toString();


        SharedPreferences myPreferences
                = PreferenceManager.getDefaultSharedPreferences(CardActivity.this);
        currency  = myPreferences.getString("currency", "usd");

        card.setCurrency(currency);
        if(firstName.length()>0 || lastName.length()>0) card.setName(firstName + " " + lastName);
        if(zipcode.length()>0) card.setAddressZip(zipcode);
        if(city.length()>0) card.setAddressCity(city);
        if(country.length()>0) card.setAddressCountry(country);
        if(state.length()>0) card.setAddressState(state);
        if(address.length()>0) card.setAddressLine1(address);

        stripe.createToken(
                card,
                new TokenCallback() {
                    public void onSuccess(Token token) {
                        // Send token to your server
                        new StripeCharge(token.getId()).execute();
                    }
                    public void onError(Exception error) {
                        // Show localized error message
                        Toast.makeText(CardActivity.this,
                                error.getLocalizedMessage(),
                                Toast.LENGTH_LONG
                        ).show();
                    }
                });
    }

    public class StripeCharge extends AsyncTask<String, Void, String> {
        String token;
        private Handler handler = new Handler();

        private ProgressDialog progress = new ProgressDialog(CardActivity.this);
        public StripeCharge(String token) {
            this.token = token;
        }

        @Override
        protected String doInBackground(String... params) {
            new Thread() {
                @Override
                public void run() {
                    // Create a new HttpClient and Post Header
                    try {
                        URL url = new URL("https://stripe.android.dongletv.com/card.php");
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setReadTimeout(10000);
                        conn.setConnectTimeout(15000);
                        conn.setRequestMethod("POST");
                        conn.setDoInput(true);
                        conn.setDoOutput(true);

                        List<NameValuePair> params = new ArrayList<NameValuePair>();
                        params.add(new NameValuePair("method", "card"));
                        params.add(new NameValuePair("source", token));
                        params.add(new NameValuePair("customer", customer));

                        OutputStream os = null;

                        os = conn.getOutputStream();
                        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                        writer.write(getQuery(params));
                        writer.flush();
                        writer.close();


                        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        String decodedString;
                        StringBuilder stringBuilder = new StringBuilder();
                        while ((decodedString = in.readLine()) != null) {
                            stringBuilder.append(decodedString);
                        }
                        in.close();
                        /*YOUR RESPONSE */
                        String json = stringBuilder.toString();
                        Log.e("response from card", json);
                        JSONObject jObj = new JSONObject(json);
                        String response = jObj.getString("response");
                        os.close();

                        // hideProgressDialog
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                // Update the progress status
                                progress.dismiss();
                            }
                        });
                        message = jObj.getString("message");
                        if(response.equals("Success")){
                            Intent chargeIntent = new Intent(CardActivity.this, ChargeActivity.class);
                            chargeIntent.putExtra("customer", customer);
                            startActivity(chargeIntent);
                            finish();
                        } else {
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(CardActivity.this, message, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                }
            }.start();
            return "Done";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("Result",s);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            progress.setCancelable(true);
            progress.setTitle("Card");
            progress.setMessage("Inputing...");
            progress.show();
        }
    }


    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }
        Log.e("Query",result.toString());
        return result.toString();
    }

}
