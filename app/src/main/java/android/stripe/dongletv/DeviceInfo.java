package android.stripe.dongletv;

import android.util.Log;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

public class DeviceInfo {
    public String getMacAddressValue() {
        String macAddress;
        try {
            macAddress=getMacAddressCurrent();
            if ( macAddress == "") {
                macAddress = getMacAddressCurrent();
            }
            if ( macAddress == "") {
                macAddress = getMacAddressLegacy();
            }
            if (macAddress == "") {
                macAddress = getMacAddressLegacyWiFi();
            }
        } catch (Exception ex) {
            Log.d("DEBUG", ex.getMessage());
            return (getMacAddressLegacyWiFi()+"-"+getBuildPropValues("ro.build.display.id"));
        }
        return macAddress;
    }
    public String getBuildPropValues(String key) {
        Process p;
        String bpResult = "";
        try {
            p = new ProcessBuilder("/system/bin/getprop", key).redirectErrorStream(true).start();
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line=br.readLine()) != null){
                bpResult = line;
            }
            p.destroy();
            Log.d("result", bpResult);
        }
        catch ( IOException e) {
            Log.d("error", e.getMessage());
            return "Unknown BuildProp Value";
        }
        return bpResult;
    }

    private String loadFileAsString(String filePath) throws java.io.IOException{
        StringBuffer fileData = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
        }
        reader.close();
        return fileData.toString();
    }

    public String getMacAddressCurrent(){
        try {
            return loadFileAsString("/sys/class/net/eth0/address").toUpperCase().substring(0, 17);
        } catch (IOException ex) {
            Log.d("DEBUG", ex.getMessage());
            return "";
        }
    }
    public String getMacAddressLegacyWiFi() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }
                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:",b));
                }
                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString().toUpperCase();
            }
        } catch (Exception ex) {
            Log.d("DEBUG", ex.getMessage());
            return "";
        }
        return "";
    }
    public static String getMacAddressLegacy() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("eth0")) continue;
                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }
                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:",b));
                }
                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString().toUpperCase();
            }
        } catch (Exception ex) {
            Log.d("DEBUG", ex.getMessage());
            return "";
        }
        return "";
    }

}
