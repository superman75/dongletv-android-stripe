package android.stripe.dongletv;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ItemsViewHolder>{
    private ArrayList<MainActivity.SimplePlan> planArrayList;
    Activity activity;
    Context context;

    public ItemsAdapter(Activity activity,ArrayList<MainActivity.SimplePlan> planArrayList){
        this.planArrayList = planArrayList;
        this.activity = activity;
        this.context = this.activity.getBaseContext();
    }

    @Override
    public ItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item, parent, false);
        ItemsViewHolder viewHolder = new ItemsViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemsViewHolder holder, final int i) {
        holder.planName.setText(planArrayList.get(i).getName());
        if(planArrayList.get(i).getDescription() != "null"){
            holder.planDesc.setText(planArrayList.get(i).getDescription());
        }
        holder.planPrice.setText(planArrayList.get(i).getAmount()+planArrayList.get(i).getCurrency());
        Picasso.with(this.context).load(planArrayList.get(i).getImage()).fit().centerCrop().into(holder.planImage);
        holder.planImage.setImageURI(Uri.parse(planArrayList.get(i).getImage()));

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.buy.performClick();
            }
        });

        holder.buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent customerIntent = new Intent(activity,CustomerActivity.class);

                SharedPreferences myPreferences
                        = PreferenceManager.getDefaultSharedPreferences(activity);
                SharedPreferences.Editor myEditor = myPreferences.edit();
                myEditor.putString("productName", planArrayList.get(i).getName());
                myEditor.putString("productDesc", planArrayList.get(i).getDescription());
                myEditor.putString("currency", planArrayList.get(i).getCurrency());
                myEditor.putString("productImage", planArrayList.get(i).getImage());
                myEditor.putInt("productPrice", planArrayList.get(i).getAmount());
                myEditor.apply();

                activity.startActivity(customerIntent);
                activity.finish();
            }
        });
    }


    @Override
    public int getItemCount() {

        return planArrayList.size();
    }


    class ItemsViewHolder extends RecyclerView.ViewHolder{

        ImageView planImage;
        TextView planName,planPrice, planDesc;
        Button buy;
        View layout;

        ItemsViewHolder(View itemView) {
            super(itemView);
            layout = itemView.findViewById(R.id.item_layout);
            planImage = itemView.findViewById(R.id.imageView);
            planName = itemView.findViewById(R.id.item_name);
            planPrice = itemView.findViewById(R.id.item_price);
            planDesc = itemView.findViewById(R.id.item_desc);
            buy = itemView.findViewById(R.id.buyButton);
        }
    }
}
