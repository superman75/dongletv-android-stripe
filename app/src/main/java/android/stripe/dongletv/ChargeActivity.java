package android.stripe.dongletv;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

public class ChargeActivity extends AppCompatActivity {

    TextView product_price, product_name, product_desc;
    ImageView product_image;
    String productName, productDesc, productImage, currency, message, customer;
    Integer productPrice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charge);

        product_price = findViewById(R.id.product_price);
        product_name = findViewById(R.id.product_name);
        product_desc = findViewById(R.id.product_description);
        product_image = findViewById(R.id.product_image);

        Bundle extra =  getIntent().getExtras();
        customer = extra.getString("customer");

        SharedPreferences myPreferences
                = PreferenceManager.getDefaultSharedPreferences(this);

        productPrice = myPreferences.getInt("productPrice", 0);
        productName = myPreferences.getString("productName","Product Name");
        productDesc = myPreferences.getString("productDesc", "Product Description");
        productImage = myPreferences.getString("productImage", "");
        currency = myPreferences.getString("currency", "usd");

        product_price.setText(String.valueOf(productPrice) + currency.toUpperCase());
        product_name.setText(productName);
        product_desc.setText(productDesc);
        Picasso.with(this).load(productImage).into(product_image);

    }

    public void charge(View view) {
        new CreateCharge().execute();
    }

    public class CreateCharge extends AsyncTask<String, Void, String>{
        private Handler handler = new Handler();

        private ProgressDialog progress = new ProgressDialog(ChargeActivity.this);

        @Override
        protected String doInBackground(String... strings) {
            new Thread(){
                @Override
                public void run() {
// Create a new HttpClient and Post Header
                    try {

                        URL url = new URL("https://stripe.android.dongletv.com/charge.php");
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setReadTimeout(10000);
                        conn.setConnectTimeout(15000);
                        conn.setRequestMethod("POST");
                        conn.setDoInput(true);
                        conn.setDoOutput(true);

                        List<NameValuePair> params = new ArrayList<NameValuePair>();
                        params.add(new NameValuePair("method", "charge"));
                        params.add(new NameValuePair("description", productName + " : " + productDesc));
                        params.add(new NameValuePair("currency", currency));
                        params.add(new NameValuePair("amount", productPrice*100 + ""));
                        params.add(new NameValuePair("customer", customer));
                        DeviceInfo deviceInfo = new DeviceInfo();
                        params.add(new NameValuePair("mac", deviceInfo.getMacAddressValue()));
                        params.add(new NameValuePair("firmware", deviceInfo.getBuildPropValues("ro.build.display.id")));
                        params.add(new NameValuePair("otaid", deviceInfo.getBuildPropValues("otaupdater.otaid")));


                        OutputStream os = null;

                        os = conn.getOutputStream();
                        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                        writer.write(getQuery(params));
                        writer.flush();
                        writer.close();


                        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        String decodedString;
                        StringBuilder stringBuilder = new StringBuilder();
                        while ((decodedString = in.readLine()) != null) {
                            stringBuilder.append(decodedString);
                        }
                        in.close();

                        /*YOUR RESPONSE */
                        String json = stringBuilder.toString();
                        JSONObject jObj = new JSONObject(json);
                        String response = jObj.getString("response");
                        os.close();

                        // hideProgressDialog
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                // Update the progress status
                                progress.dismiss();
                            }
                        });
                        if(response.equals("Success")){
                            Intent intent = new Intent(ChargeActivity.this, ConfirmActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            message = jObj.getString("message");
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(ChargeActivity.this, message, Toast.LENGTH_LONG).show();
                                }
                            });
                        }

                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }


                }
            }.start();
            return "Done";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            progress.setCancelable(true);
            progress.setTitle("Charge");
            progress.setMessage("Progress...");
            progress.show();

        }
    }
    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }
        Log.e("Query",result.toString());
        return result.toString();
    }

}
